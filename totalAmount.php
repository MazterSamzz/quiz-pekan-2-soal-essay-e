SELECT
	customers.name AS customer_name, SUM(amount) total_amount
FROM
	orders INNER JOIN customers
	ON orders.customer_id = customers.id
GROUP BY
	customer_name
ORDER BY total_amount ASC